import os
import pygame
import time

import detect


class Screen:
    """Will initialize to full screen if on the pi, otherwise open a 640x480 window for testing"""
    screen = None

    def __init__(self):
        if detect.is_pi():
            os.putenv('SDL_FBDEV', '/dev/fb0')
            os.putenv('SDL_NOMOUSE', '1')
            os.putenv('SDL_VIDEODRIVER', 'fbcon')

        pygame.display.init()

        size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
        print "Framebuffer size: %d x %d" % (size[0], size[1])

        if detect.is_pi():
            self.screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
        else:
            self.screen = pygame.display.set_mode((640, 480))

        pygame.font.init()

    def test(self):
        # Fill the screen with red (255, 0, 0)
        red = (255, 0, 0)
        self.screen.fill(red)
        # Update the display
        pygame.display.update()

if __name__ == '__main__':
    sc = Screen()
    sc.test()
    time.sleep(10)
