import platform


def is_pi():
    if platform.system().startswith('Linux'):
        if platform.machine().startswith('arm'):
            return True

    return False
